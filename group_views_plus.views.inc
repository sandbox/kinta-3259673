<?php

/**
 * @file
 * Contains Views hooks.
 */

/**
 * Implements hook_views_data_alter().
 */
function group_views_plus_views_data_alter(array &$data) {
  $data['node_field_data']['nid']['title'] = t('Node-Nid');
  $data['groups_field_data']['group_content_representative'] = [
    'relationship' => [
      'title' => t('Representative node by group relationship content entity criteria'),
      'label'  => t('Representative node by relationship content entity criteria'),
      'help' => t('Obtains a single representative node for each group, according to a chosen group_relationship content sort criterion.--'),
      'id' => 'group_relationship_groupwise_max',
      'relationship field' => 'id',
      'outer field' => 'groups_field_data.id',
      'argument table' => 'groups_field_data',
      'argument field' => 'id',
      'base'   => 'group_relationship_field_data',
      'field'  => 'id',
      'relationship' => 'group_relationship_field_data:gid',
    ],
  ];
  $data['groups_field_data']['group_content_representative_gc'] = [
    'relationship' => [
      'title' => t('Representative node'),
      'label'  => t('Representative node'),
      'help' => t('Obtains a single representative node for each group, according to a chosen group_relationship sort criterion.'),
      'id' => 'groupwise_max',
      'relationship field' => 'id',
      'outer field' => 'groups_field_data.id',
      'argument table' => 'groups_field_data',
      'argument field' => 'id',
      'base'   => 'group_relationship_field_data',
      'field'  => 'id',
      'relationship' => 'group_relationship_field_data:gid',
    ],
  ];
}
